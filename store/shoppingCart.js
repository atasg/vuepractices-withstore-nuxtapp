import axios from "axios";

export const state = () => ({
    products: [],
    cartItemsCount: 0,
    cart:[],
})

export const getters = {
    products: state => state.products,
    cart:state => state.cart
}

export const mutations = {
    setProducts(state,payload){
        state.products = payload
    },
    increment(state) {
        state.count++
    },
    addToCart(state, payload) {
        state.cart = payload
    },
    removeFromCart(state,payload) {
        state.cart = payload
    },
    clearProducts(state,payload) {
        state.cart = payload
    },
    getCart(state,payload) {
        state.cart = payload
    }
}

export const actions = {
    async setProducts({commit}){
        const response = await axios.get("http://127.0.0.1:8080/shoppingcart/allproducts")
        commit("setProducts",response.data.data)
    },
    async getCart({commit}){
        const response = await axios.get("http://127.0.0.1:8080/shoppingcart/getcart",{withCredentials: true })
        commit("getCart",response.data.cart)
    },
    async addToCart({commit},payload) {
        const id = payload
        const uri = "http://127.0.0.1:8080/shoppingcart/addproducttocart/id:"
        const response = await axios.get(`${uri}${id}`,{withCredentials: true })
        commit("addToCart",response.data.cart)
    },
    async removeFromCart({commit},payload) {
        const id = payload
        const uri = "http://127.0.0.1:8080/shoppingcart/removeproductfromcart/id:"
        const response = await axios.get(`${uri}${id}`,{withCredentials: true })
        commit("addToCart",response.data.cart)
    },
    async clearProducts({commit}){
        const response = await axios.get("http://127.0.0.1:8080/shoppingcart/deletecart",{withCredentials: true })
        commit("clearProducts",response.data.cart)
    }
}