import axios from "axios"
export const state = () => ({
    ledgerBook: []
})

export const mutations = {
    setContent(state, payload) {
        state.ledgerBook = payload
    },
    newContent(state, payload) {
        state.ledgerBook.push(payload)
    }
}
export const actions = {
    async newContent({commit}, payload) {
        const response = await axios.post("http://127.0.0.1:8080/ledgerbook/addmessage/", payload)
        commit("newContent", response.data.data)
    },
    async setContent({commit}) {
        axios.get("http://127.0.0.1:8080/ledgerbook/allmessages/").then((res) => {
            commit("setContent",res.data.data)

        })
    }
    
}